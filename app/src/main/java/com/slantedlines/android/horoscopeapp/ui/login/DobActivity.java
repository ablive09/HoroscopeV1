package com.slantedlines.android.horoscopeapp.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.slantedlines.android.horoscopeapp.BottomNavActivity;
import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.MyHoroscopeData;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_user.LoginResponse;

import java.util.Calendar;
import java.util.Date;

public class DobActivity extends AppCompatActivity implements DatePicker.OnDateChangedListener {
    public static final String TAG = "DobActivity";
    Button buttonSubmit;
    private DatePicker datePicker;
    private int day, month, year;
    private MySessionManager mySessionManager;
    private LoginViewModel loginViewModel;
    private AdView mAdView;
    public static final String PRIVACY_POLICY_URL = "https://horoscope.slantedlines.com/privacy-policy/";
    private TextView textViewPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dob);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        textViewPolicy = findViewById(R.id.privacy_policy);
        textViewPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PRIVACY_POLICY_URL));
                startActivity(browserIntent);
            }
        });
        buttonSubmit = findViewById(R.id.loginButton);
        buttonSubmit.setTextColor(getResources().getColor(R.color.datewhite));

        //Setting Date Picker View in UI
        setCurrentDateOnView();

        mySessionManager = new MySessionManager(this);
        if (mySessionManager.isLoggedIn())
            Log.i(TAG, "Logged in");
        if (mySessionManager.isAppRegistered())
            Log.i(TAG, mySessionManager.getApiToken() + "");
        else {
            Log.i(TAG, "Not registered");

        }
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mySessionManager.isAppRegistered()) {
                    loginViewModel.registerApp().observe(DobActivity.this, new Observer<LoginResponse>() {
                        @Override
                        public void onChanged(LoginResponse loginResponse) {
                            if (loginResponse != null) {
                                mySessionManager.setAppRegistered();
                                mySessionManager.createLoginSession(loginResponse);
                                mySessionManager.setLogin(true);
                                grantAccessToApp();
                            }
                        }
                    });
                } else {
                    mySessionManager.setLogin(true);
                    grantAccessToApp();
                }
            }
        });

    }

    private void grantAccessToApp() {
        if (mySessionManager.isLoggedIn()) {
            MyHoroscopeData myHoroscopeData = new MyHoroscopeData(this);
            myHoroscopeData.findHoroscope(year, month, day);
            mySessionManager.setHoroscope(myHoroscopeData.getmHoroscope());
            Toast.makeText(DobActivity.this, "Hey! " + myHoroscopeData.getmHoroscope(), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(DobActivity.this, BottomNavActivity.class);
            startActivity(intent);
            finish();
        }
    }


    public void setCurrentDateOnView() {


        datePicker = findViewById(R.id.dpresult);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        // set current date into datepicker
        datePicker.init(year, month, day, DobActivity.this);
        datePicker.setMaxDate(new Date().getTime());
    }


    @Override
    public void onDateChanged(DatePicker view, int yearr, int monthOfYear, int dayOfMonth) {
        year = view.getYear();
        month = view.getMonth() + 1;
        day = view.getDayOfMonth();

//        Log.i(TAG, String.format("%s-%s-%s", year, month, day));

    }
}
