package com.slantedlines.android.horoscopeapp.ui.horoscope;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class NewViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "NewViewPagerAdapter";
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public NewViewPagerAdapter(FragmentManager manager) {
        super(manager);
        Log.i(TAG, manager.getBackStackEntryCount() + "");
        if (manager.getBackStackEntryCount() > 2)
            manager.popBackStack();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public void addFrag(Fragment fragment) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add("");
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void addFrag(Fragment fragment, String title, Bundle bundle) {
        fragment.setArguments(bundle);
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void removeFrag() {
        mFragmentList.clear();
    }


}
