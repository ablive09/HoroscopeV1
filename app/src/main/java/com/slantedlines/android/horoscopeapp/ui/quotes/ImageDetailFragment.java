package com.slantedlines.android.horoscopeapp.ui.quotes;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */

public class ImageDetailFragment extends Fragment {
    private static final int PERMISSION_REQUEST_CODE = 120;
    private static final String TAG = "ImageDetailFragment";
    OnImageCloseListener listener;
    String imageUrl, fileName, dirPath;
    int downloadId;
    File targetFolder;
    LinearLayout imageViewDownload, imageViewShare;
    private ImageView imageViewFull, imageViewCancel;
    MySessionManager mySessionManager;

    public ImageDetailFragment() {
        // Required empty public constructor
    }

    public ImageDetailFragment(OnImageCloseListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageViewFull = view.findViewById(R.id.imageview_full_view);
        imageViewCancel = view.findViewById(R.id.imageview_cancel);
        imageViewDownload = view.findViewById(R.id.image_download);
        imageViewShare = view.findViewById(R.id.image_share);

        mySessionManager = new MySessionManager(getContext());

        Bundle bundle = getArguments();
        imageUrl = bundle.getString("image_url");
        Picasso.with(getContext())
                .load(Uri.parse(imageUrl))
                .placeholder(R.drawable.placeholder)
                .into(imageViewFull);

        imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImageClosed();
            }
        });

        imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, imageUrl);
                startActivity(Intent.createChooser(shareIntent, "Share image using"));

            }
        });

        imageViewDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    new DownloadTask().execute(stringToURL());
                } else {
                    requestPermission();
                }
            }
        });
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new DownloadTask().execute(stringToURL());
                } else {
                    Toast.makeText(getContext(), "Permission required to save images", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public interface OnImageCloseListener {
        void onImageClosed();
    }

    protected URL stringToURL() {
        URL url;
        try {
            url = new URL(imageUrl);
            return url;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveTempBitmap(Bitmap bitmap) {
        if (isExternalStorageWritable()) {
            saveImage(bitmap);
        } else {
            //prompt the user or do something
            Log.i(TAG, "saveTempBitmap: External storage is not writable");
        }
    }

    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_quotes");
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "IMG_" + timeStamp + ".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            if (!mySessionManager.getStorageLocated())
                displayMessage();
            else
                Toast.makeText(getContext(), getString(R.string.download_complete), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.alert_storage_location));
        builder.setPositiveButton(getString(R.string.string_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mySessionManager.setStorageLocated(true);
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private class DownloadTask extends AsyncTask<URL, Void, Bitmap> {

        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute: ");
        }

        protected Bitmap doInBackground(URL... urls) {
            URL url = urls[0];
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                return BitmapFactory.decodeStream(bufferedInputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result) {
            // Hide the progress dialog
            Log.i(TAG, "onPostExecute: ");
            if (result != null) {
                Log.i(TAG, "bitmap not Null: ");
                saveTempBitmap(result);
            } else {
                // Notify user that an error occurred while downloading image
                Log.i(TAG, "bitmap null: ");
            }
        }
    }
}
