package com.slantedlines.android.horoscopeapp.model_horoscope;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BirthDay {

    @SerializedName("birthdayhoroscope")
    @Expose
    private List<BirthdayHoroscope> birthdayhoroscope = null;

    public List<BirthdayHoroscope> getBirthdayhoroscope() {
        return birthdayhoroscope;
    }

    public void setBirthdayhoroscope(List<BirthdayHoroscope> birthdayhoroscope) {
        this.birthdayhoroscope = birthdayhoroscope;
    }

}