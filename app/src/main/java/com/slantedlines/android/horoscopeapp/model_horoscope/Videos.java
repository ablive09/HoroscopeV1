package com.slantedlines.android.horoscopeapp.model_horoscope;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Videos {

    @SerializedName("urlyoutube")
    @Expose
    private List<Urlyoutube> urlyoutube = null;

    public List<Urlyoutube> getUrlyoutube() {
        return urlyoutube;
    }

    public void setUrlyoutube(List<Urlyoutube> urlyoutube) {
        this.urlyoutube = urlyoutube;
    }

}