package com.slantedlines.android.horoscopeapp.ui.horoscope;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_horoscope.YearlyHoroscope;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class YearlyFragment extends Fragment {
    Calendar myCalendar;
    MySessionManager mySessionManager;
    ProgressBar progressBar;
    TextView textViewYearly;
    private DashboardViewModel dashboardViewModel;

    public YearlyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_yearly, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        myCalendar = Calendar.getInstance();
        mySessionManager = new MySessionManager(getContext());

        progressBar = view.findViewById(R.id.yearly_progress_bar);
        textViewYearly = view.findViewById(R.id.tv_yearly);

        progressBar.setVisibility(View.VISIBLE);
        textViewYearly.setVisibility(View.GONE);

        String thisYear = Integer.toString(myCalendar.get(Calendar.YEAR));
        String yearlyHorosSlug = String.format("%s%s", mySessionManager.getHoroscope(), thisYear);

        if (mySessionManager.getTemporaryFetch()) {
            loadData(mySessionManager.getTemporaryHoros() + thisYear);
        } else {
            loadData(mySessionManager.getHoroscope() + thisYear);
        }
    }

    public void loadData(String yearlyHorosSlug) {
        dashboardViewModel.getYearlyHoroscope(yearlyHorosSlug);
        dashboardViewModel.getYearlyObservable().observe(getViewLifecycleOwner(), new Observer<YearlyHoroscope>() {
            @Override
            public void onChanged(YearlyHoroscope yearlyHoroscope) {
                textViewYearly.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                textViewYearly.setText(yearlyHoroscope.getYearlyhoroscope());
            }
        });
    }

}
