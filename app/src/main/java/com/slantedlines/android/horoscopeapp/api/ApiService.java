package com.slantedlines.android.horoscopeapp.api;

import com.slantedlines.android.horoscopeapp.model_horoscope.BirthDay;
import com.slantedlines.android.horoscopeapp.model_horoscope.Daily;
import com.slantedlines.android.horoscopeapp.model_horoscope.Wallpapers;
import com.slantedlines.android.horoscopeapp.model_horoscope.Yearly;
import com.slantedlines.android.horoscopeapp.model_user.LoginResponse;
import com.slantedlines.android.horoscopeapp.model_horoscope.Videos;
import com.slantedlines.android.horoscopeapp.model_user.VersionResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
//    @POST("register")
//    Call<RegisterResponse> register(@Field("name") String name,
//                                    @Field("email") String email,
//                                    @Field("password") String password,
//                                    @Field("password_confirmation") String passwordConfirmation);

    @POST("login")
    Call<LoginResponse> login(@Query("email") String email,
                              @Query("password") String password);

    @GET("checkversion/{version}")
    Call<VersionResponse> checkVersion(@Path("version") Integer version);

    @GET("dailyhoroscope/{slug}")
    Call<Daily> getDailyHoroscope(@Path("slug") String slug);

    @GET("yearlyhoroscope/{slug}")
    Call<Yearly> getYearlyHoroscope(@Path("slug") String slug);

    @GET("bdayhoroscope/{today_date}")
    Call<BirthDay> getBirthdayHoroscope(@Path("today_date") String slug);


    @GET("wallpaper/{zodiac}")
    Call<Wallpapers> getWallpapers(@Path("zodiac") String zodiacName);

    @GET("videoslink/{zodiac}")
    Call<Videos> getVideos(@Path("zodiac") String zodiacName);
}