package com.slantedlines.android.horoscopeapp.model_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("registerDetails")
    @Expose
    private RegisterDetails registerDetails;
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    public RegisterDetails getRegisterDetails() {
        return registerDetails;
    }

    public void setRegisterDetails(RegisterDetails registerDetails) {
        this.registerDetails = registerDetails;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}