package com.slantedlines.android.horoscopeapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.slantedlines.android.horoscopeapp.api.ApiClient;
import com.slantedlines.android.horoscopeapp.api.ApiService;
import com.slantedlines.android.horoscopeapp.helper.InternetDetector;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_user.VersionResponse;
import com.slantedlines.android.horoscopeapp.ui.login.DobActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    private static final String TAG = "SplashScreen";
    public static final int version = 8;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if (new InternetDetector(this).hasNetworkConnection()) {
            checkForVersionValidation();
        } else {
            displayMessageDialog(getString(R.string.no_internet));
        }
    }

    private void checkForVersionValidation() {
        Call<VersionResponse> callWallpapers = ApiClient.createService(ApiService.class).checkVersion(version);
        callWallpapers.enqueue(new Callback<VersionResponse>() {
            @Override
            public void onResponse(Call<VersionResponse> call, Response<VersionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getVersion().equalsIgnoreCase("True")) {
                        proceedWithLogin();
                    } else {
                        displayAppUpdate();
                    }
                } else {
                    displayMessageDialog(getString(R.string.connection_error));
                }
                Log.i(TAG, "onResponse code: " + response.code());
            }

            @Override
            public void onFailure(Call<VersionResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: versioncheck", t);
                displayMessageDialog(getString(R.string.connection_error));
            }
        });
    }

    private void proceedWithLogin() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MySessionManager mySessionManager = new MySessionManager(SplashScreen.this);
                if (mySessionManager.isAppRegistered() && mySessionManager.isLoggedIn()) {
                    Intent i = new Intent(SplashScreen.this, BottomNavActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(SplashScreen.this, DobActivity.class);
                    startActivity(i);
                }
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void displayMessageDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.string_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        builder.show();
    }

    private void displayAppUpdate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_update));
        builder.setMessage(getString(R.string.update_app_now));
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setPositiveButton(getString(R.string.string_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openPlayStoreLink();
                        finish();
                    }
                });
        builder.show();
    }

    private void openPlayStoreLink() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
