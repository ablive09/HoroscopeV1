package com.slantedlines.android.horoscopeapp.ui.horoscope;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_horoscope.BirthdayHoroscope;
import com.slantedlines.android.horoscopeapp.model_horoscope.DailyHoroscope;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "TodayFragment";
    Calendar myCalendar;
    MySessionManager mySessionManager;
    ProgressBar progressBar;
    TextView textViewDaily, textViewBornToday, textViewLabelBornToday;

    private DashboardViewModel dashboardViewModel;
    String dailyHorosSlug, bdayHorosSlug;
    String thisYear, thisMonth, day;
    private ChangeViewModel changeViewModel;

    public TodayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_today, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);

        myCalendar = Calendar.getInstance();
        mySessionManager = new MySessionManager(getContext());

        textViewDaily = view.findViewById(R.id.tv_daily);
        textViewBornToday = view.findViewById(R.id.tv_bday);
        textViewLabelBornToday = view.findViewById(R.id.label_bday);

        progressBar = view.findViewById(R.id.today_progress_bar);

        textViewDaily.setVisibility(View.GONE);
        textViewBornToday.setVisibility(View.GONE);
        textViewLabelBornToday.setVisibility(View.GONE);

        thisYear = Integer.toString(myCalendar.get(Calendar.YEAR));
        thisMonth = Integer.toString(myCalendar.get(Calendar.MONTH) + 1);
        day = Integer.toString(myCalendar.get(Calendar.DAY_OF_MONTH));

        if ((Integer.parseInt(thisMonth)) < 10)
            thisMonth = "0" + thisMonth;
        if (Integer.parseInt(day) < 10)
            day = "0" + day;

        dailyHorosSlug = String.format("%s%s%s", thisYear, thisMonth, day);
        bdayHorosSlug = String.format("%s-%s-%s", thisYear, thisMonth, day);

        if (mySessionManager.getTemporaryFetch()) {
            loadData(mySessionManager.getTemporaryHoros() + dailyHorosSlug);
        } else {
            loadData(mySessionManager.getHoroscope() + dailyHorosSlug);
        }

        dashboardViewModel.getDailyObservable().observe(getViewLifecycleOwner(), new Observer<DailyHoroscope>() {
            @Override
            public void onChanged(DailyHoroscope dailyHoroscope) {
                progressBar.setVisibility(View.GONE);
                textViewDaily.setVisibility(View.VISIBLE);
                textViewDaily.setText(dailyHoroscope.getDailyhoroscope());
            }
        });

        dashboardViewModel.getBirthdayObservable().observe(getViewLifecycleOwner(), new Observer<BirthdayHoroscope>() {
            @Override
            public void onChanged(BirthdayHoroscope birthdayHoroscope) {
                progressBar.setVisibility(View.GONE);
                textViewBornToday.setVisibility(View.VISIBLE);
                textViewLabelBornToday.setVisibility(View.VISIBLE);
                textViewBornToday.setText(birthdayHoroscope.getBirthdayhoroscope());
            }
        });

    }

    public void loadData(String dailyHorosSlug) {
        dashboardViewModel.getDailyHoroscope(dailyHorosSlug);
        dashboardViewModel.getBirthdayHoroscope(bdayHorosSlug);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Toast.makeText(getContext(), mySessionManager.getHoroscope(), Toast.LENGTH_SHORT).show();
        String dailyHorosSlug = String.format("%s%s%s%s", mySessionManager.getHoroscope(), "2020", "05", "06");
        dashboardViewModel.getDailyHoroscope(dailyHorosSlug);
        dashboardViewModel.getDailyObservable().observe(this, new Observer<DailyHoroscope>() {
            @Override
            public void onChanged(DailyHoroscope dailyHoroscope) {
                progressBar.setVisibility(View.GONE);
                textViewDaily.setVisibility(View.VISIBLE);
                textViewDaily.setText(dailyHoroscope.getDailyhoroscope());
            }
        });
    }


}
