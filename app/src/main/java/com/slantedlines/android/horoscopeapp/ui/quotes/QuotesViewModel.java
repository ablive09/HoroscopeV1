package com.slantedlines.android.horoscopeapp.ui.quotes;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Constraints;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.api.ApiClient;
import com.slantedlines.android.horoscopeapp.api.ApiService;
import com.slantedlines.android.horoscopeapp.helper.InternetDetector;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_horoscope.Wallpapers;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotesViewModel extends AndroidViewModel {

    private static final String TAG = "QuotesViewModel";
    private MutableLiveData<Wallpapers> data;
    private Context context;
    private MySessionManager mySessionManager;
    private Wallpapers urlWallpaper = new Wallpapers();

    public QuotesViewModel(@NonNull Application application) {
        super(application);
        this.data = new MutableLiveData<>();
        this.context = application;
        this.mySessionManager = new MySessionManager(context);

    }


    public LiveData<Wallpapers> getWallPapers(String zodiacName) {
            Call<Wallpapers> callWallpapers = ApiClient.createService(ApiService.class, mySessionManager.getApiToken()).getWallpapers(zodiacName);
            callWallpapers.enqueue(new Callback<Wallpapers>() {
                @Override
                public void onResponse(Call<Wallpapers> call, Response<Wallpapers> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getUrlwallpaper().size() > 0) {
                            urlWallpaper = response.body();
                            Log.i(TAG, urlWallpaper.toString());
                            data.setValue(urlWallpaper);
                        } else {
                            Toast.makeText(context, context.getString(R.string.no_data), Toast.LENGTH_LONG).show();
                            data.setValue(null);
                        }

                    } else {
                        Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                        Log.i(Constraints.TAG, "ResponseCode=" + response.code());
                        data.setValue(null);
                    }
                }


                @Override
                public void onFailure(Call<Wallpapers> call, Throwable t) {
//                    Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure: ", t);
                    data.setValue(null);
                }
            });
        return data;
    }

/*
    public LiveData<Wallpapers> initImageBitmaps(){

        Log.d(Constraints.TAG, "initImageBitmaps: preparing bitmaps.");

        UrlWallpaper wallpaper=new UrlWallpaper();
        List<UrlWallpaper> wallpaperArrayList=new ArrayList<>();

        UrlWallpaper wallpaper1=new UrlWallpaper();
        wallpaper1.setUrl("https://images.unsplash.com/photo-1569229569803-69384f5efa83?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80");
        wallpaperArrayList.add(wallpaper1);

        UrlWallpaper wallpaper2=new UrlWallpaper();
        wallpaper2.setUrl("https://images.unsplash.com/photo-1488381397757-59d6261610f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=332&q=80");
        wallpaperArrayList.add(wallpaper2);

        UrlWallpaper wallpaper3=new UrlWallpaper();
        wallpaper3.setUrl("https://images.unsplash.com/photo-1565287267132-e1461a7b7c78?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80");
        wallpaperArrayList.add(wallpaper3);

        UrlWallpaper wallpaper4=new UrlWallpaper();
        wallpaper4.setUrl("https://images.unsplash.com/photo-1562398766-0f40f880a33a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=405&q=80");
        wallpaperArrayList.add(wallpaper4);


        UrlWallpaper wallpaper5=new UrlWallpaper();
        wallpaper5.setUrl("https://images.unsplash.com/photo-1534515729281-5ddf2c470538?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80");
        wallpaperArrayList.add(wallpaper5);

        UrlWallpaper wallpaper6=new UrlWallpaper();
        wallpaper6.setUrl("https://images.unsplash.com/photo-1578909196327-4dad2ddae8eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=375&q=80");
        wallpaperArrayList.add(wallpaper6);


        UrlWallpaper wallpaper7=new UrlWallpaper();
        wallpaper7.setUrl("https://images.unsplash.com/photo-1562164979-6fc780665354?ixlib=rb-1.2.1&auto=format&fit=crop&w=748&q=80");
        wallpaperArrayList.add(wallpaper7);

        UrlWallpaper wallpaper8=new UrlWallpaper();
        wallpaper8.setUrl("https://images.unsplash.com/photo-1515288207449-100e125abccb?ixlib=rb-1.2.1&auto=format&fit=crop&w=663&q=80");
        wallpaperArrayList.add(wallpaper8);

        UrlWallpaper wallpaper9=new UrlWallpaper();
        wallpaper.setUrl("https://images.unsplash.com/photo-1534196511436-921a4e99f297?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80");
        wallpaperArrayList.add(wallpaper);

        urlWallpaper.setUrlwallpaper(wallpaperArrayList);
        data.setValue(urlWallpaper);

        return data;
    }
    */

}