package com.slantedlines.android.horoscopeapp.model_horoscope;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wallpapers {

    @SerializedName("urlwallpaper")
    @Expose
    private List<UrlWallpaper> urlwallpaper = null;

    public List<UrlWallpaper> getUrlwallpaper() {
        return urlwallpaper;
    }

    public void setUrlwallpaper(List<UrlWallpaper> urlwallpaper) {
        this.urlwallpaper = urlwallpaper;
    }

}