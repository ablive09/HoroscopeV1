package com.slantedlines.android.horoscopeapp.model_horoscope;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyHoroscope {

    @SerializedName("dailyhoroscope")
    @Expose
    private String dailyhoroscope;

    public String getDailyhoroscope() {
        return dailyhoroscope;
    }

    public void setDailyhoroscope(String dailyhoroscope) {
        this.dailyhoroscope = dailyhoroscope;
    }

}