package com.slantedlines.android.horoscopeapp.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.slantedlines.android.horoscopeapp.R;

public class InternetDetector {

    Context mContext;

    public InternetDetector(Context mContext) {
        this.mContext = mContext;
    }

    public boolean hasNetworkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if (!isConnected)
            Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        return isConnected;
    }
}
