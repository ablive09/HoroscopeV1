package com.slantedlines.android.horoscopeapp.ui.quotes;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.slantedlines.android.horoscopeapp.BottomNavActivity;
import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.InternetDetector;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_horoscope.Wallpapers;
import com.squareup.picasso.Picasso;

import java.nio.FloatBuffer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class QuotesFragment extends Fragment implements StaggeredRecyclerAdapter.OnImageClickListener, ImageDetailFragment.OnImageCloseListener {

    private static final String TAG = "QuotesFragment";
    private static final int NUM_COLUMNS = 2;
    private QuotesViewModel quotesViewModel;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    String thisYear;
    private TextView textView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String horoscopeName;
    private MySessionManager mySessionManager;
    private Calendar myCalendar;
    private Bundle args;
    InternetDetector internetDetector;
    private AdView mAdView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        quotesViewModel =
                ViewModelProviders.of(this).get(QuotesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_quotes, container, false);
//        final TextView textView = root.findViewById(R.id.text_home);
        swipeRefreshLayout = root.findViewById(R.id.quotes_refresh);
        recyclerView = root.findViewById(R.id.recycler_view);
        progressBar = root.findViewById(R.id.quotes_progress_bar);
        textView = root.findViewById(R.id.quotes_status);

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        textView.setVisibility(View.GONE);

        mAdView = root.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mySessionManager = new MySessionManager(getContext());
        internetDetector = new InternetDetector(getContext());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                fetchData();
            }
        });
        fetchData();
        return root;
    }

    private void fetchData() {
        if (internetDetector.hasNetworkConnection()) {
            String horoscopeName;
            if (mySessionManager.getTemporaryFetch())
                horoscopeName = mySessionManager.getTemporaryHoros();
            else
                horoscopeName = mySessionManager.getHoroscope();

            Log.i(TAG, horoscopeName);

            quotesViewModel.getWallPapers(horoscopeName).observe(getViewLifecycleOwner(), new Observer<Wallpapers>() {
                @Override
                public void onChanged(Wallpapers wallpapers) {
                    if (swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);

                    if (wallpapers == null) {
                        progressBar.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(getContext().getString(R.string.no_data));
                    } else {
                        textView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        StaggeredRecyclerAdapter staggeredRecyclerViewAdapter =
                                new StaggeredRecyclerAdapter(getContext(), wallpapers.getUrlwallpaper(), QuotesFragment.this);
                        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
                        recyclerView.setItemViewCacheSize(20);
                        recyclerView.setDrawingCacheEnabled(true);
                        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                        recyclerView.setLayoutManager(staggeredGridLayoutManager);
                        recyclerView.setAdapter(staggeredRecyclerViewAdapter);
                    }
                }
            });
        } else {
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            textView.setText(getContext().getString(R.string.no_internet));
        }
    }

    @Override
    public void onImageClicked(String imageUrl) {
        args = new Bundle();
        args.putString("image_url", imageUrl);
        attachFragment();
    }

    private void attachFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag("ImageDetailFragment");
        if (fragment != null) {
            fragment.setArguments(args);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.attach(fragment);
            fragmentTransaction.commit();
        } else {
            loadFragment();
        }
    }

    private void loadFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment fragment = new ImageDetailFragment(QuotesFragment.this);
        fragment.setArguments(args);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, "ImageDetailFragment");
        fragmentTransaction.commit();
    }

    public void closeImageDetailFragment() {
        // Get the FragmentManager.
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag("ImageDetailFragment");
        if (fragment != null) {
            // Create and commit the transaction to remove the fragment.
            FragmentTransaction fragmentTransaction =
                    fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment).commit();
        }
    }

    @Override
    public void onImageClosed() {
        closeImageDetailFragment();
        fetchData();
    }
}