package com.slantedlines.android.horoscopeapp.helper;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.slantedlines.android.horoscopeapp.R;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by agrud on 9/28/15.
 */
public class FileDownload extends
        AsyncTask<String, String, String> {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_ACCESS = 101;
    Context mcontext;
    private ProgressDialog pDialog;

    public FileDownload(Context context) {

        this.mcontext = context;
        hasWritePermissions();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Showing progress dialog
        pDialog = new ProgressDialog(mcontext);
        pDialog.setMessage(mcontext.getString(R.string.downloading));
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
//        pDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mcontext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
        pDialog.show();

    }


    /**
     * Downloading file in background thread
     */
    @Override
    protected String doInBackground(String... params) {
        int count;
        try {
            String save_path = params[1];
            URL url = new URL(params[0]);
            Log.i("Download", url.getPath());
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file
            OutputStream output = new FileOutputStream(save_path);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.i("Download ", e.getMessage());
        }

        return null;
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
        pDialog.setProgress(Integer.parseInt(progress[0]));
    }

    /**
     * After completing background task
     * Dismiss the progress dialog
     **/
    @Override
    protected void onPostExecute(String result) {
        // Dismiss the progress dialog
        if (pDialog.isShowing())
            pDialog.dismiss();
        Toast.makeText(mcontext, mcontext.getString(R.string.download_complete),
                Toast.LENGTH_LONG).show();
    }

    private void hasWritePermissions() {
        if (ContextCompat.checkSelfPermission(mcontext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions((Activity) mcontext,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_ACCESS);

        } else {
            // Permission has already been granted
        }
    }

}
