package com.slantedlines.android.horoscopeapp.model_horoscope;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BirthdayHoroscope {

    @SerializedName("birthdayhoroscope")
    @Expose
    private String birthdayhoroscope;

    public String getBirthdayhoroscope() {
        return birthdayhoroscope;
    }

    public void setBirthdayhoroscope(String birthdayhoroscope) {
        this.birthdayhoroscope = birthdayhoroscope;
    }

}