package com.slantedlines.android.horoscopeapp.model_horoscope;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Daily {

    @SerializedName("dailyhoroscope")
    @Expose
    private List<DailyHoroscope> dailyhoroscope = null;

    public List<DailyHoroscope> getDailyhoroscope() {
        return dailyhoroscope;
    }

    public void setDailyhoroscope(List<DailyHoroscope> dailyhoroscope) {
        this.dailyhoroscope = dailyhoroscope;
    }

}