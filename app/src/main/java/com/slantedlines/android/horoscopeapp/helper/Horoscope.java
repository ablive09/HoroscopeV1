package com.slantedlines.android.horoscopeapp.helper;

public class Horoscope {

    private String mHoroscopeName;
    private String mHoroscopeDateRange;

    public String getmHoroscopeName() {
        return mHoroscopeName;
    }

    public void setmHoroscopeName(String mHoroscopeName) {
        this.mHoroscopeName = mHoroscopeName;
    }

    public String getmHoroscopeDateRange() {
        return mHoroscopeDateRange;
    }

    public void setmHoroscopeDateRange(String mHoroscopeDateRange) {
        this.mHoroscopeDateRange = mHoroscopeDateRange;
    }
}
