package com.slantedlines.android.horoscopeapp.model_horoscope;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearlyHoroscope {

    @SerializedName("yearlyhoroscope")
    @Expose
    private String yearlyhoroscope;

    public String getYearlyhoroscope() {
        return yearlyhoroscope;
    }

    public void setYearlyhoroscope(String yearlyhoroscope) {
        this.yearlyhoroscope = yearlyhoroscope;
    }

}