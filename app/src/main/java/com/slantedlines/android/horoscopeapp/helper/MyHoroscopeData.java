package com.slantedlines.android.horoscopeapp.helper;

import android.content.Context;

import com.slantedlines.android.horoscopeapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aashis on 7/19/2017.
 */

public class MyHoroscopeData {
    Horoscope horoscope;
    Context context;
    private String mHoroscope, mHoroscopeDate;

    public MyHoroscopeData(Context context) {
        this.context = context;
    }

    public List<Horoscope> getAllHoroscopes() {
        List<Horoscope> horoscopeList = new ArrayList<>();
        horoscopeList.add(getAries());
        horoscopeList.add(getTaurus());
        horoscopeList.add(getGemini());
        horoscopeList.add(getCancer());
        horoscopeList.add(getLeo());
        horoscopeList.add(getVirgo());
        horoscopeList.add(getLibra());
        horoscopeList.add(getScorpio());
        horoscopeList.add(getSagittarius());
        horoscopeList.add(getCapricorn());
        horoscopeList.add(getAquarius());
        horoscopeList.add(getPisces());

        return horoscopeList;
    }

    public Horoscope getMyHoroscope(String horoscopeName) {
        switch (horoscopeName.toUpperCase()) {
            case "ARIES":
                horoscope = getAries();
                break;
            case "TAURUS":
                horoscope = getTaurus();
                break;
            case "GEMINI":
                horoscope = getGemini();
                break;
            case "CANCER":
                horoscope = getCancer();
                break;
            case "LEO":
                horoscope = getLeo();
                break;
            case "VIRGO":
                horoscope = getVirgo();
                break;
            case "LIBRA":
                horoscope = getLibra();
                break;
            case "SCORPIO":
                horoscope = getScorpio();
                break;
            case "SAGITTARIUS":
                horoscope = getSagittarius();
                break;
            case "CAPRICORN":
                horoscope = getCapricorn();
                break;
            case "AQUARIUS":
                horoscope = getAquarius();
                break;
            case "PISCES":
                horoscope = getPisces();
                break;
        }
        return horoscope;
    }

    public Horoscope getAries() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_aries));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_aries));
        return horoscope;
    }

    public Horoscope getTaurus() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_taurus));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_taurus));
        return horoscope;
    }

    public Horoscope getGemini() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_gemini));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_gemini));
        return horoscope;
    }

    public Horoscope getCancer() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_cancer));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_cancer));
        return horoscope;
    }

    public Horoscope getLeo() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_leo));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_leo));
        return horoscope;
    }

    public Horoscope getVirgo() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_virgo));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_virgo));
        return horoscope;
    }

    public Horoscope getLibra() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_libra));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_libra));
        return horoscope;
    }

    public Horoscope getScorpio() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_scorpio));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_scorpio));
        return horoscope;
    }

    public Horoscope getSagittarius() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_sagittarius));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_sagittarius));
        return horoscope;
    }

    public Horoscope getCapricorn() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_capricorn));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_capricon));
        return horoscope;
    }

    public Horoscope getAquarius() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_aquarius));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_aquarius));
        return horoscope;
    }

    public Horoscope getPisces() {
        Horoscope horoscope = new Horoscope();
        horoscope.setmHoroscopeName(context.getString(R.string.string_pisces));
        horoscope.setmHoroscopeDateRange(context.getString(R.string.range_pisces));
        return horoscope;
    }

    public String getRatings(Integer rating) {
        switch (rating) {
            case 1: {
                return "★";
            }

            case 2: {
                return "★★";
            }
            case 3: {
                return "★★★";
            }
            case 4: {
                return "★★★★";
            }
            case 5: {
                return "★★★★★";
            }
        }
        return null;
    }


    public String getmHoroscope() {
        return mHoroscope;
    }

    public void setmHoroscope(String mHoroscope) {
        this.mHoroscope = mHoroscope;
    }

    public String getmHoroscopeDate() {
        return mHoroscopeDate;
    }

    public void setmHoroscopeDate(String mHoroscopeDate) {
        this.mHoroscopeDate = mHoroscopeDate;
    }


    public void findHoroscope(int year, int month, int day) {
        if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 1 && day <= 19)) {
            mHoroscope = "Capricorn";
            mHoroscopeDate = "(Dec 22 - Jan 19)";
        } else if ((month == 1 && day >= 20 && day <= 31) || (month == 2 && day >= 1 && day <= 17)) {
            mHoroscope = "Aquarius";
            mHoroscopeDate = "(Jan 20 - Feb 17)";
        } else if ((month == 2 && day >= 18 && day <= 29) || (month == 3 && day >= 1 && day <= 19)) {
            mHoroscope = "Pisces";
            mHoroscopeDate = "(Feb 18 - Mar 19)";
        } else if ((month == 3 && day >= 21 && day <= 31) || (month == 4 && day >= 1 && day <= 19)) {
            mHoroscope = "Aries";
            mHoroscopeDate = "(Mar 21 - Apr 19)";
        } else if ((month == 4 && day >= 20 && day <= 30) || (month == 5 && day >= 1 && day <= 20)) {
            mHoroscope = "Taurus";
            mHoroscopeDate = "(Apr 20 - May 20)";
        } else if ((month == 5 && day >= 21 && day <= 31) || (month == 6 && day >= 1 && day <= 20)) {
            mHoroscope = "Gemini";
            mHoroscopeDate = "(May 21 - Jun 20)";
        } else if ((month == 6 && day >= 21 && day <= 30) || (month == 7 && day >= 1 && day <= 22)) {
            mHoroscope = "Cancer";
            mHoroscopeDate = "(Jun 21 - Jul 22)";
        } else if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 1 && day <= 22)) {
            mHoroscope = "Leo";
            mHoroscopeDate = "(Jul 23 - Aug 22)";
        } else if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 1 && day <= 22)) {
            mHoroscope = "Virgo";
            mHoroscopeDate = "(Aug 23 - Sep 22)";
        } else if ((month == 9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22)) {
            mHoroscope = "Libra";
            mHoroscopeDate = "(Sep 23 - Oct 22)";
        } else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21)) {
            mHoroscope = "Scorpio";
            mHoroscopeDate = "(Oct 23 - Nov 21)";
        } else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21)) {
            mHoroscope = "Sagittarius";
            mHoroscopeDate = "(Nov 22 - Dec 21)";
        }
    }

}